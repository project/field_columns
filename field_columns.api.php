<?php

/**
 * Implements hook_field_columns_alter().
 *
 * Alter a field's column definitions before it's checked against the previous
 * version and the db is altered. Now it the time to change something.
 *
 * BEWARE: if you change ANYTHING after the column's been created, it will be
 * dropped and recreated. Don't change trivial things here, like 'size' or sorting.
 *
 * @see field_columns_field_update_field()
 * @see _field_columns_column_specs()
 */
function hook_field_columns_alter(&$columns, $field, $prior_field) {
  if ($field['field_name'] == 'field_my_field') {
    if (isset($columns['name_prefix'])) {
      $columns['name_prefix']['size'] = 60;
    }
  }
}

/**
 * Implements hook_field_columns_views_data_alter().
 *
 * Your int field might be a boolean, or you might want to remove the argument
 * option from your funky column.
 *
 * This hook is called once for every field column.
 *
 * @see _views_fetch_data_build()
 */
function hook_field_columns_views_data_alter(&$column_data, $context) {
  if ($context['field_name'] == 'field_my_field' && $context['column']['name'] == 'is_active') {
    $column_data['field']['handler'] = 'views_handler_field_boolean';
  }
}
