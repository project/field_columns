<?php

/**
 * Implements hook_views_data_alter().
 */
function field_columns_views_data_alter(&$data) {
  $state = variable_get('field_columns_state', array());

  foreach ($state as $field_name => $columns) {
    $table_name = 'field_data_' . $field_name;
    if (isset($data[$table_name])) {
      $field_data = &$data[$table_name];

      foreach ($columns as $column_name => $column) {
        $title = $field_data[$field_name]['title'] . ': ' . $column_name;
        $title_short = $field_data[$field_name]['title short'] . ': ' . $column_name;

        $views_field_name = $field_name . '_' . $column_name;
        $base = array(
          'field' => $views_field_name,
          'real field' => $views_field_name,
          'field_name' => $field_name,
          'table' => $table_name,
        );

        $field_data[$views_field_name] = array(
          'group' => 'Field columns',
          'title' => $title,
          'title short' => $title_short,
          'help' => $column_name . ' - ' . $field_data[$field_name]['help'],
          'field' => array(
            'handler' => 'views_handler_field_numeric',
          ),
          'argument' => array(
            'handler' => 'views_handler_argument_numeric',
          ) + $base,
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ) + $base,
          'sort' => array(
            'handler' => 'views_handler_sort',
          ) + $base,
        );

        // If it's not a number, it's a string.
        if (!in_array($column['type'], array('int', 'float'))) {
          $field_data[$views_field_name]['field']['handler'] = 'views_handler_field';
          $field_data[$views_field_name]['argument']['handler'] = 'views_handler_argument_string';
          $field_data[$views_field_name]['filter']['handler'] = 'views_handler_filter_string';
        }

        // Let the module defining these custom fields customize the Views integration.
        $context = compact('field_name', 'column');
        drupal_alter('field_columns_views_data', $field_data[$views_field_name], $context);
      }
    }
  }
}
